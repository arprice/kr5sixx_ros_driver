/**
 * \file KR5Kinematics.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 8 20, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "kuka_kr5sixx_driver/KR5Kinematics.h"
#include <iostream>
#include <assert.h>

namespace kr5
{

Eigen::Matrix3d dhRotation(const double alpha, const double thetaOffset, const double theta)
{
	Eigen::Matrix3d rot;
	double thetaAct = thetaOffset + theta;

	double cosT = cos(thetaAct);
	double sinT = sin(thetaAct);
	double cosA = cos(alpha);
	double sinA = sin(alpha);

	rot << cosT, -sinT*cosA, sinT*sinA,
		   sinT, cosT*cosA, -cosT*sinA,
		   0, sinA, cosA;

	return rot;
}

void wristIK(const Eigen::Vector3d& goal, std::vector<std::array<double,3> >& jointAngles)
{
	jointAngles.clear();

	// Simplified Variable Aliases
	const double a = r2;
	const double b = rho;
	const double d = goal.z()-d1;

	// Compute first joint
	double theta1[2];
	theta1[0] = atan2(goal.y(),goal.x());
	if (theta1[0] > 0)
	{
		theta1[1] = theta1[0] - M_PI;
	}
	else
	{
		theta1[1] = theta1[0] + M_PI;
	}

	// For Shoulder Forward and Backward
	for (int i = 0; i < 2; ++i)
	{
#ifdef ENFORCE_JOINT_LIMITS
		if (theta1[i] < minAngle[0] || theta1[i] > maxAngle[0]) { continue; }
#endif
		double c = sqrt(goal.x()*goal.x()+goal.y()*goal.y()) + (0==i ? -r1 : r1);

		double tau = atan2(c,d);
		double ssquared = c*c+d*d;
		double s = sqrt(ssquared);

		// Check for workspace singularities via triangle inequality
		if (s > a+b) { continue; }
		if (a > s+b || b > s+a)  { continue; }

		double theta2[2], theta3[2];
		theta3[0] = acos((ssquared-(a*a+b*b))/(2*a*b));
		assert(theta3[0] == theta3[0]); // NaN check
		theta3[1] = -theta3[0];

		// For elbow up and down
		for (int j = 0; j < 2; ++j)
		{
#ifdef ENFORCE_JOINT_LIMITS
			if (theta3[j]-phi < minAngle[2] || theta3[j]-phi > maxAngle[2]) { continue; }
#endif
			const double theta3i = theta3[j];
			double c3 = cos(theta3i);
			double s3 = sin(theta3i);
			double ct = cos(tau);

			double alpha = a+(b*c3);
			double beta = -b*s3;
			double gamma = s*ct;

			double sign = (0 == i ? 1 : -1);

			theta2[j] = 2*atan2(beta+sign*sqrt(beta*beta + alpha*alpha - gamma*gamma), alpha + gamma);

#ifdef ENFORCE_JOINT_LIMITS
			if (theta2[j] < minAngle[1] || theta2[j] > maxAngle[1]) { continue; }
#endif

			std::array<double,3> currentAngles;
			currentAngles[0] = theta1[i];
			currentAngles[1] = theta2[j];
			currentAngles[2] = theta3[j]-phi; // Switch to our joint convention
			jointAngles.push_back(currentAngles);
		}
	}
}


void IK(const Eigen::Isometry3d& goal, std::vector<Vector6d>& jointAngles)
{
	jointAngles.clear();
	Eigen::Matrix3d toolRot = goal.rotation().matrix();
	Eigen::Vector3d wristGoal = goal.translation()-t6*toolRot.col(0);
	std::vector<std::array<double,3> > armAngles;
	wristIK(wristGoal, armAngles);
	const int wristSolutions = armAngles.size();
	if (wristSolutions < 1) { return; } // No solutions found to wrist target

	for (int i = 0; i < wristSolutions; ++i)
	{
		// Compute orientation of first links
		// Seemingly Random Y-rotation is because D-H always rotates about its z-axis,
		// but we want the orientation that is identity when at home position.
		Eigen::Matrix3d armRot = kr5::dhRotation(kr5::alpha1,kr5::theta1,armAngles[i][0]) *
								 kr5::dhRotation(kr5::alpha2,kr5::theta2,armAngles[i][1]) *
								 kr5::dhRotation(kr5::alpha3,kr5::theta3,armAngles[i][2]) *
								 Eigen::AngleAxisd(-M_PI/2.0, Eigen::Vector3d::UnitY());

		// Compute rotation from arm to hand
		Eigen::Matrix3d wristRot = armRot.transpose() * toolRot;

		// Do both wrist orientations
		for (int j = 0; j < 2; ++j)
		{
			// Compute target joint angles
			double sign = (0 == j) ? 1 : -1; // Flip to get both sides of the +/- sqrt
			double beta = atan2(sign*sqrt(wristRot(0,1)*wristRot(0,1)+wristRot(0,2)*wristRot(0,2)),wristRot(0,0));
			double sinb = sin(beta);

			double alpha, gamma;
			if (0 == beta) // Redundant singularity...
			{
				alpha = 0;
				gamma = atan2(-wristRot(1,2),wristRot(2,2));
			}
			else
			{
				alpha = atan2(wristRot(1,0)/sinb,-wristRot(2,0)/sinb);
				gamma = atan2(wristRot(0,1)/sinb,wristRot(0,2)/sinb);
			}

#ifdef ENFORCE_JOINT_LIMITS
			if (alpha < minAngle[3] || alpha > maxAngle[3]) { continue; }
			if (beta < minAngle[4] || beta > maxAngle[4]) { continue; }
			if (gamma < minAngle[5] || gamma > maxAngle[5]) { continue; }
#endif

			// Add to solution
			Vector6d angles;
			for (int j = 0; j < 3; ++j) { angles[j] = armAngles[i][j]; }
			angles[3] = alpha;
			angles[4] = beta;
			angles[5] = gamma;
			jointAngles.push_back(angles);

			if (0 == beta) // Redundant singularity...
			{
				break;
			}
		}
	}
}

void filterByLimits(std::vector<Vector6d>& jointAngles)
{
	for (int i = 0; i < jointAngles.size(); ++i)
	{
		for (int j = 0; j < 6; ++j)
		{
			if (jointAngles[i][j] < minAngle[j] || jointAngles[i][j] > maxAngle[j])
			{
				jointAngles.erase(jointAngles.begin() + i);
				--i;
				break;
			}
		}
	}
}

} // namespace kr5
