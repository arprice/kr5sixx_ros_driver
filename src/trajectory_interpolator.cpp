/**
 * \file trajectory_interpolator.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 2014-11-16
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <Eigen/Geometry>

#include <moveit_msgs/DisplayTrajectory.h>

#include "kuka_kr5sixx_driver/KR5Kinematics.h"
#include "kuka_kr5sixx_driver/interpolation.h"
#include "kuka_kr5sixx_driver/Viterbi.h"

//#define GREEDY_INTERPOLATE

const double DENSE_TIMESTEP = 0.012;
//const double DENSE_TIMESTEP = 0.125; // useful for testing
const std::string EE_NAME = "flange_pose";

const Eigen::Vector6d weights = Eigen::Vector6d((Eigen::Vector6d() << 2.0,2.0,2.0,3.0,2.0,1.0).finished());

namespace Eigen
{
typedef Matrix<double, 6, 1> Vector6d;
}


double element(const geometry_msgs::Vector3& v, size_t n)
{
	if (0 == n)
	{ return v.x; }
	else if (1 == n)
	{ return v.y; }
	else if (2 == n)
	{ return v.z; }
	else
	{
		throw std::exception();
	}
	return 0;
}

double stateCost(const Eigen::Vector6d& q)
{
	return q.array().abs().matrix().dot(weights);
}

double transitionCost(const Eigen::Vector6d& q1, const Eigen::Vector6d& q2)
{
	double wDist = (q1-q2).array().abs().matrix().dot(weights);
	return wDist*wDist;
}

ros::Publisher jointTrajPub;
ros::Subscriber jointTrajSub, poseTrajSub;
ros::Publisher moveitPub;

void joint_traj_callback(const trajectory_msgs::JointTrajectoryConstPtr& sparseTraj)
{
	trajectory_msgs::JointTrajectory denseTraj;
	denseTraj.joint_names = sparseTraj->joint_names;
	const int numJoints = denseTraj.joint_names.size();

	trajectory_msgs::JointTrajectoryPoint p;
	p.positions.resize(numJoints);
	p.velocities.resize(numJoints);
	p.accelerations.resize(numJoints);

	for (int i = 1; i < sparseTraj->points.size(); ++i)
	{
		trajectory_msgs::JointTrajectoryPoint a = sparseTraj->points[i-1];
		trajectory_msgs::JointTrajectoryPoint b = sparseTraj->points[i];

		if (a.velocities.size() != a.positions.size()) { a.velocities.resize(a.positions.size(), 0); }
		if (b.velocities.size() != b.positions.size()) { b.velocities.resize(b.positions.size(), 0); }

		const double T = (b.time_from_start - a.time_from_start).toSec();
		const int numSteps = static_cast<int>((T/DENSE_TIMESTEP)) + 1;

		std::vector<std::vector<Eigen::Vector3d> > partialTrajectories;
		partialTrajectories.resize(numJoints);

		// Compute trajectories for individual joints
		for (int j = 0; j < numJoints; ++j)
		{
			Eigen::Vector2d x0(a.positions[j], a.velocities[j]);
			Eigen::Vector2d xT(b.positions[j], b.velocities[j]);
//			partialTrajectories[j] = sisoCubicSpline(x0, xT, T, DENSE_TIMESTEP);
			partialTrajectories[j] = sisoQuinticSpline(x0, xT, T, DENSE_TIMESTEP);
		}

		// Assemble into full trajectory
		for (int k = 0; k < numSteps; ++k)
		{
			for (int j = 0; j < numJoints; ++j)
			{
				p.time_from_start = a.time_from_start + ros::Duration(static_cast<double>(k)*DENSE_TIMESTEP);
				p.positions[j] = (partialTrajectories[j][k][0]);
				p.velocities[j] = (partialTrajectories[j][k][1]);
				p.accelerations[j] = (partialTrajectories[j][k][2]);
			}
			denseTraj.points.push_back(p);
		}
	}

	// Make sure we get the last point
	denseTraj.points.push_back(sparseTraj->points[sparseTraj->points.size()-1]);

	denseTraj.header.frame_id = sparseTraj->header.frame_id;
	denseTraj.header.stamp = sparseTraj->header.stamp;
	jointTrajPub.publish(denseTraj);

	moveit_msgs::DisplayTrajectory dt;
	moveit_msgs::RobotTrajectory rt;
	rt.joint_trajectory = denseTraj;
	dt.trajectory.push_back(rt);

	moveitPub.publish(dt);
}

void pose_traj_callback(const trajectory_msgs::MultiDOFJointTrajectoryConstPtr& sparseTraj)
{
	trajectory_msgs::JointTrajectory denseTraj;
	if (sparseTraj->joint_names.size() != 1 || sparseTraj->joint_names[0] != EE_NAME)
	{
		ROS_ERROR_STREAM("Invalid joint name in sparse pose trajectory. Should be '" << EE_NAME << "'.");
		return;
	}
	denseTraj.joint_names.push_back("shoulder_yaw");
	denseTraj.joint_names.push_back("shoulder_pitch");
	denseTraj.joint_names.push_back("elbow_pitch");
	denseTraj.joint_names.push_back("elbow_roll");
	denseTraj.joint_names.push_back("wrist_pitch");
	denseTraj.joint_names.push_back("wrist_roll");

	std::vector<Eigen::Vector6d> ikSolutions;
#ifdef GREEDY_INTERPOLATE
	Eigen::Vector6d qPrev = Eigen::Vector6d::Zero();
#else
	std::vector<std::vector<Eigen::Vector6d> > solutionTrellis;
#endif


	const int numJoints = 6;

	trajectory_msgs::JointTrajectoryPoint p;
	p.positions.resize(numJoints);
	p.velocities.resize(numJoints);
	p.accelerations.resize(numJoints);

	for (int i = 1; i < sparseTraj->points.size(); ++i)
	{
		trajectory_msgs::MultiDOFJointTrajectoryPoint a = sparseTraj->points[i-1];
		trajectory_msgs::MultiDOFJointTrajectoryPoint b = sparseTraj->points[i];

		if (a.velocities.size() != a.transforms.size()) { a.velocities.resize(a.transforms.size()); }
		if (b.velocities.size() != b.transforms.size()) { b.velocities.resize(b.transforms.size()); }

		const double T = (b.time_from_start - a.time_from_start).toSec();
		const int numSteps = static_cast<int>((T/DENSE_TIMESTEP)) + 1;

		std::vector<std::vector<Eigen::Vector3d> > partialTrajectories;
		partialTrajectories.resize(3);

		// Compute trajectories for individual position elements
		for (int j = 0; j < 3; ++j)
		{
			Eigen::Vector2d x0(element(a.transforms[0].translation,j), element(a.velocities[0].linear,j));
			Eigen::Vector2d xT(element(b.transforms[0].translation,j), element(b.velocities[0].linear,j));
//			partialTrajectories[j] = sisoCubicSpline(x0, xT, T, DENSE_TIMESTEP);
			partialTrajectories[j] = sisoQuinticSpline(x0, xT, T, DENSE_TIMESTEP);
		}
//		std::vector<Eigen::Vector3d> splineTime = sisoCubicSpline(Eigen::Vector2d(0,0), Eigen::Vector2d(1,0), T, DENSE_TIMESTEP);
		std::vector<Eigen::Vector3d> splineTime = sisoQuinticSpline(Eigen::Vector2d(0,0), Eigen::Vector2d(1,0), T, DENSE_TIMESTEP);

		Eigen::Quaterniond qA; const geometry_msgs::Quaternion& rotA = a.transforms[0].rotation;
		qA.x() =rotA.x; qA.y() =rotA.y; qA.z() =rotA.z; qA.w() =rotA.w;
		Eigen::Quaterniond qB; const geometry_msgs::Quaternion& rotB = b.transforms[0].rotation;
		qB.x() =rotB.x; qB.y() =rotB.y; qB.z() =rotB.z; qB.w() =rotB.w;
		qA.normalize();
		qB.normalize();

		// Assemble into pose trajectory and perform IK to get joint trajectory
		for (int k = 0; k < numSteps; ++k)
		{
			Eigen::Isometry3d pose = Eigen::Isometry3d::Identity();
			// Trajectories are in [x,y,z][step][p,v,a]
			pose.translate(Eigen::Vector3d(partialTrajectories[0][k][0], partialTrajectories[1][k][0], partialTrajectories[2][k][0]));
			pose.rotate(qA.slerp(splineTime[k][0], qB));

			kr5::IK(pose, ikSolutions);
			if (ikSolutions.size() < 1)
			{
				ROS_ERROR_STREAM("Unable to solve IK for pose:\n" << pose.matrix());
				return;
			}
#ifdef GREEDY_INTERPOLATE
			// Sort solutions by distance to qPrev
			kr5::SolutionSort sorter(qPrev, weights);//(Eigen::Vector6d::Zero(), weights);
			std::sort(ikSolutions.begin(), ikSolutions.end(), sorter);
			Eigen::Vector6d q = ikSolutions[0];

			p.time_from_start = a.time_from_start + ros::Duration(static_cast<double>(k)*DENSE_TIMESTEP);
			for (int j = 0; j < numJoints; ++j)
			{
				p.positions[j] = q[j];
				p.velocities[j] = 0;//(partialTrajectories[j][k][1]);
				p.accelerations[j] = 0;//(partialTrajectories[j][k][2]);
			}
			denseTraj.points.push_back(p);

			qPrev = q;
#else
			// Add solutions to this step of the trellis
			solutionTrellis.push_back(ikSolutions);

			// Save the time, we'll fill out the position after we find the optimal path
			p.time_from_start = a.time_from_start + ros::Duration(static_cast<double>(k)*DENSE_TIMESTEP);
			denseTraj.points.push_back(p);
#endif

			ikSolutions.clear();
		}
	}

#ifndef GREEDY_INTERPOLATE
	std::vector<int> cheapestPath = viterbi(solutionTrellis, &stateCost, &transitionCost, 1000);
	if (cheapestPath.size() < 1)
	{
		ROS_ERROR_STREAM("Unable to find a path that satisfies the maximum transition cost.");
		return;
	}
	assert(cheapestPath.size() == denseTraj.points.size());
	for (int i = 0; i < cheapestPath.size(); ++i)
	{
		trajectory_msgs::JointTrajectoryPoint& pt = denseTraj.points[i];
		const Eigen::Vector6d& q = solutionTrellis[i][cheapestPath[i]];
		for (int j = 0; j < numJoints; ++j)
		{
			pt.positions[j] = q[j];
		}
	}
#endif

	// Make sure we get the last point
	//denseTraj.points.push_back(sparseTraj->points[sparseTraj->points.size()-1]);

	denseTraj.header.frame_id = sparseTraj->header.frame_id;
	denseTraj.header.stamp = sparseTraj->header.stamp;
	jointTrajPub.publish(denseTraj);

	moveit_msgs::DisplayTrajectory dt;
	moveit_msgs::RobotTrajectory rt;
	rt.joint_trajectory = denseTraj;
	dt.trajectory.push_back(rt);

	moveitPub.publish(dt);
}


int main(int argc, char** argv)
{
	ros::init (argc, argv, "trajectory_interpolator");
	ros::NodeHandle nh;

	jointTrajSub = nh.subscribe("/sparse_trajectory", 1, &joint_traj_callback);
	poseTrajSub = nh.subscribe("/pose_trajectory", 1, &pose_traj_callback);
	jointTrajPub = nh.advertise<trajectory_msgs::JointTrajectory>("/dense_trajectory", 1 , true);
	moveitPub = nh.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1 , true);

	ros::spin();
	
	return 0;
}
