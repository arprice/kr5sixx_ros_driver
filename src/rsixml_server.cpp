/**
 * \file rsixml_server.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 2014-11-14
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>

#include <boost/asio.hpp>

#include <ros/ros.h>
#include <ros/package.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <control_msgs/JointControllerState.h>
#include <sensor_msgs/JointState.h>

#include <tinyxml.h>

#include <Eigen/Geometry>

using boost::asio::ip::tcp;

// Constants for Communication and Parsing
const int KUKA_PORT = 6008;
const std::string DEFAULT_FROM_KUKA = "package://kuka_kr5sixx_driver/FromKRC.xml";
const std::string DEFAULT_TO_KUKA = "package://kuka_kr5sixx_driver/ToKRC.xml";

const int ROBOT_DOF = 6;
const std::string CLOSING_TAG = "</Rob>";
const std::array<std::string, ROBOT_DOF> KUKA_JOINT_NAMES = {"A1", "A2", "A3", "A4", "A5", "A6"};
const std::array<std::string, ROBOT_DOF> KUKA_CARTESIAN_NAMES = {"X", "Y", "Z", "A", "B", "C"};

// TODO: There must be an option to toggle to joint-degree control from motor-degree control
const std::array<double, ROBOT_DOF> JOINT_MOTOR_SCALE = {80.0, 100.0, 80.0, 80.0, 80.0, 40.5};//1 joint deg = x motor deg
const std::array<double, ROBOT_DOF> CMD_MOTOR_SCALE = {1.4, 1.74, 1.4, 1.4, 1.4, 0.87}; //1 cmd deg = x motor deg

const std::array<double, ROBOT_DOF> URDF_JOINT_DIRECTIONS = {-1, 1, 1, -1, 1, -1};
const std::array<double, ROBOT_DOF> URDF_JOINT_OFFSETS = {0, M_PI/2.0, -M_PI/2.0, 0, 0, 0};
const std::vector<std::string> URDF_JOINT_NAMES = {"shoulder_yaw", "shoulder_pitch", "elbow_pitch", "elbow_roll", "wrist_pitch", "wrist_roll"};

const double MAX_DX = 50.0;
const double MAX_DV = 50.0;
const double DT = 0.012;

namespace Eigen
{
typedef Matrix<double, 6, 1> Vector6d;
}

struct RobotState
{
public:
	std::chrono::steady_clock::time_point serverTime;
	Eigen::Vector6d joints;
	Eigen::Vector6d velocities;
	Eigen::Isometry3d pose;
	Eigen::Vector6d currents;
};

class RsiXmlServer
{
public:


};

//#define DEBUG

std::string parsePackageURL(const std::string url)
{
	std::string filename = url;
	filename.erase(0, strlen("package://"));
	size_t pos = filename.find("/");
	if (pos != std::string::npos)
	{
		std::string package = filename.substr(0, pos);
		filename.erase(0, pos);
		std::string package_path = ros::package::getPath(package);
		filename = package_path + filename;
	}

	return filename;
}

// Double-buffered trajectories for speedy switching
trajectory_msgs::JointTrajectory jtA, jtB;
trajectory_msgs::JointTrajectory* jtActive;
std::mutex trajMutex;
int trajCounter = 0;
Eigen::Vector6d setpoint = Eigen::Vector6d::Zero();

// Contains the reported joint positions and currents
RobotState robotActual;

void trajCallback(const trajectory_msgs::JointTrajectoryConstPtr& denseTraj)
{
	// Select the inactive buffer
	trajectory_msgs::JointTrajectory* jtFill;
	bool aIsActive = (jtActive == &jtA);
	if (aIsActive)
	{
		jtFill = &jtB;
	}
	else
	{
		jtFill = &jtA;
	}

	// Fill the back buffer with the new trajectory
	*jtFill = *denseTraj;

	// Switch buffers
	trajMutex.lock();
	if (aIsActive)
	{
		jtActive = &jtB;
	}
	else
	{
		jtActive = &jtA;
	}
	trajCounter = 0;
	trajMutex.unlock();
}

void controlThread()
{
	// Load template command from file
	TiXmlDocument kukaCmd, kukaStatus;
	if (!kukaCmd.LoadFile(parsePackageURL(DEFAULT_TO_KUKA)))
	{
		std::cerr << "Unable to load '" << DEFAULT_TO_KUKA << "'." << std::endl;
		return;
	}

	TiXmlElement* cmdDataRoot = kukaCmd.RootElement()->FirstChildElement("Dat");
	// TODO: add parse_error code here

//	RobotState robotActual; //robotSetpoint
	Eigen::Vector6d prevJoints = Eigen::Vector6d::Zero() * NAN;

	boost::asio::io_service ioSvc;
	tcp::acceptor acceptor(ioSvc, tcp::endpoint(tcp::v4(), KUKA_PORT));
	while(ros::ok())
	{
		// Initialize socket and wait for connections
		tcp::socket socket(ioSvc);
		std::cerr << "Waiting for connection..." << std::endl;
		acceptor.accept(socket);

		trajCounter = 0;

		while(ros::ok())
		{
//#ifdef DEBUG
			std::cerr << "Waiting for data..." << std::endl;
//#endif
			ros::spinOnce();
			// Wait for data, then read until we find the terminating text
			boost::asio::streambuf request ;
			size_t len;
			try
			{
				len = boost::asio::read_until(socket, request, CLOSING_TAG);
			}
			catch (boost::system::system_error error)//(boost::system::error_code error)
			{
				if (error.code() == boost::asio::error::eof)
				{
					std::cerr << "Client signed off." << std::endl;
					break; // Connection closed cleanly by peer.
				}
				else if (error.code() == boost::asio::error::connection_reset ||
				         error.code() == boost::asio::error::connection_aborted)
				{
					std::cerr << "Client died." << std::endl;
					break; // Connection closed but not cleanly by peer.
				}
				else
					throw boost::system::system_error(error); // something else
			}

			std::string reqString((std::istreambuf_iterator<char>(&request)), std::istreambuf_iterator<char>());

			// Read feedback from robot
			kukaStatus.Clear();
			kukaStatus.Parse(reqString.c_str());

#ifdef DEBUG
			TiXmlPrinter printer2;
			kukaStatus.Accept(&printer2);
			std::cerr << "************Request:" << std::endl;
			std::cerr << printer2.CStr() << std::endl;
#endif

			TiXmlElement* statusDataRoot = kukaStatus.RootElement()->FirstChildElement("Dat");
			TiXmlElement* statusJointAct = statusDataRoot->FirstChildElement("AIPos");
			TiXmlElement* statusCurrent = statusDataRoot->FirstChildElement("MACur");
			// TODO: add parse_error code here

			std::string ipoc((statusDataRoot->FirstChildElement("IPOC")->GetText()));

			prevJoints = robotActual.joints;
			robotActual.serverTime = std::chrono::steady_clock::now();
			for (int i = 0; i < ROBOT_DOF; ++i)
			{
				double val = 0;
				if (TIXML_SUCCESS != statusJointAct->QueryDoubleAttribute(KUKA_JOINT_NAMES[i], &(val))) { return; }
				robotActual.joints[i] = val;
				statusCurrent->QueryDoubleAttribute(KUKA_JOINT_NAMES[i], &(val));
				robotActual.currents[i] = val;
			}
			if (!isnan(prevJoints[0]))
			{
				robotActual.velocities = (robotActual.joints - prevJoints) / DT;
			}
			else
			{
				robotActual.velocities = Eigen::Vector6d::Zero();
			}
			// TODO: Compute Cartesian Position from xyz,abc


			// Update the command to incorporate the feedback
			cmdDataRoot->FirstChildElement("IPOC")->FirstChild()->SetValue(ipoc);
			if (trajCounter < jtActive->points.size())
			{
				std::cerr << "Going to traj" << std::endl;
				trajMutex.lock();
				const trajectory_msgs::JointTrajectoryPoint& jtPoint = jtActive->points[trajCounter];
				for (int j = 0; j < ROBOT_DOF; ++j)
				{
					setpoint[j] = jtPoint.positions[j];
				}
				++trajCounter;
				trajMutex.unlock();
			}

			for (int j = 0; j < ROBOT_DOF; ++j)
			{
				// Compute joint value in KUKA (not URDF) framework
				double kukaJointPos = (setpoint[j] - URDF_JOINT_OFFSETS[j]) * URDF_JOINT_DIRECTIONS[j];
				double degJointPos = kukaJointPos * 180.0/M_PI; // Convert to degrees
				double dX = (degJointPos-robotActual.joints[j]); // Get error signal
				//dX *= JOINT_MOTOR_SCALE[j] / CMD_MOTOR_SCALE[j]; // Screwy control scale
				dX = std::min(fabs(dX), fabs(MAX_DX)) * ((dX < 0) ? -1 : 1);
//				dX = std::min(fabs(dX), fabs(MAX_DV-robotActual.velocities[j])*DT) * ((dX < 0) ? -1 : 1);
				cmdDataRoot->FirstChildElement("AKorr")->SetDoubleAttribute(KUKA_JOINT_NAMES[j], dX);
			}

			TiXmlPrinter printer;
#ifndef DEBUG
			printer.SetIndent("");
			printer.SetLineBreak("");
#endif
			kukaCmd.Accept(&printer);
			std::string response(printer.CStr());

#ifdef DEBUG
			std::cerr << "************Response:" << std::endl;
			std::cerr << response << std::endl;
#endif

			boost::system::error_code ignored_error;
			boost::asio::write(socket, boost::asio::buffer(response), boost::asio::transfer_all(), ignored_error);
		}


	}
}

int main(int argc, char** argv)
{
	ros::init (argc, argv, "rsixml_server");
	ros::NodeHandle nh;

	jtActive = &jtA;

	ros::Subscriber trajSub = nh.subscribe("/dense_trajectory", 1, &trajCallback);
	ros::Publisher jsPub = nh.advertise<sensor_msgs::JointState>("/joint_states", 1);

	sensor_msgs::JointState js;
	js.header.frame_id = "/base_link";
	js.name = URDF_JOINT_NAMES;
	js.position.resize(6);
	js.effort.resize(6);

	std::thread control(controlThread);

	ros::Rate r(10);
	while (ros::ok())
	{
		for (int i = 0; i < ROBOT_DOF; ++i)
		{
			js.position[i] = ((robotActual.joints[i] * M_PI/180.0) / URDF_JOINT_DIRECTIONS[i]) + URDF_JOINT_OFFSETS[i];
			js.effort[i] = robotActual.currents[i];
		}
		js.header.stamp = ros::Time::now();
		jsPub.publish(js);

		ros::spinOnce();
		r.sleep();
	}

	return 0;
}
