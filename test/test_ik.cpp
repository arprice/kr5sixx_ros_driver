/**
 * \file test_ik.cpp
 * \brief
 *
 * \author Andrew Price
 * \date 8 20, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include "kuka_kr5sixx_driver/KR5Kinematics.h"

#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>

ros::Publisher jsPub;

void wristRotation(const Eigen::Matrix3d& wristRot, std::vector<std::array<double,3> >& wristAngles)
{
	// Do both wrist orientations
	for (int j = 0; j < 2; ++j)
	{
		// Compute target joint angles
		double sign = (0 == j) ? 1 : -1; // Flip to get both sides of the +/- sqrt
		double beta = atan2(sign*sqrt(wristRot(0,1)*wristRot(0,1)+wristRot(0,2)*wristRot(0,2)),wristRot(0,0));
		double sinb = sin(beta);

		double alpha, gamma;

		if (0 == beta) // Redundant singularity...
		{
			alpha = 0;
			gamma = atan2(-wristRot(1,2),wristRot(2,2));
		}
		else
		{
			alpha = atan2(wristRot(1,0)/sinb,-wristRot(2,0)/sinb);
			gamma = atan2(wristRot(0,1)/sinb,wristRot(0,2)/sinb);
		}

		assert(alpha == alpha);
		assert(beta == beta);
		assert(gamma == gamma);

		// Add to solution
		std::array<double,3> angles;
		angles[0] = alpha;
		angles[1] = beta;
		angles[2] = gamma;
		wristAngles.push_back(angles);

		if (0 == beta) // Redundant singularity...
		{
			break;
		}
	}
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "test_kr5_ik");
	ros::NodeHandle nh;

	jsPub = nh.advertise<sensor_msgs::JointState>("joint_states", 1);
	tf::TransformBroadcaster tb;

	sensor_msgs::JointState js;
	js.header.frame_id = "/base_link";
	js.name.push_back("shoulder_yaw");
	js.name.push_back("shoulder_pitch");
	js.name.push_back("elbow_pitch");
	js.name.push_back("elbow_roll");
	js.name.push_back("wrist_pitch");
	js.name.push_back("wrist_roll");
	js.position.resize(6,0);

	tf::StampedTransform transform;
	transform.frame_id_="/base_link";
	transform.child_frame_id_="/goal";
	transform.setIdentity();
//	transform.setOrigin(tf::Vector3(kr5::r1+kr5::d4, 0.0, kr5::d1+kr5::r2+kr5::r3));
//	transform.setOrigin(tf::Vector3(kr5::r1+kr5::d4, -0.3, 0.3));
//	transform.setOrigin(tf::Vector3(kr5::r1+kr5::d4, 0.0, kr5::d1));
	transform.setOrigin(tf::Vector3(kr5::d4, 0.2, kr5::d1+kr5::r2));
//	transform.setOrigin(tf::Vector3(-0.3, 0.2, kr5::d1-0.001-0.2));
//	transform.setOrigin(tf::Vector3(0.2, 0.0, -0.2));

	transform.setOrigin(tf::Vector3(kr5::d4, 0.0, kr5::d1+kr5::r2));

//	std::cout << Eigen::AngleAxisd(-M_PI/2.0, Eigen::Vector3d::UnitY()).matrix() << std::endl;
	Eigen::Quaterniond q(Eigen::AngleAxisd(-M_PI/8.0, Eigen::Vector3d::UnitZ()));
	q = q * Eigen::AngleAxisd(-M_PI/8.0, Eigen::Vector3d::UnitY());
//	Eigen::Quaterniond q(Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()));
	transform.setRotation(tf::Quaternion(q.x(), q.y(), q.z(), q.w()));


	tf::StampedTransform wristTF;
	wristTF.frame_id_="/base_link";
	wristTF.child_frame_id_="/wrist_orientation";
	wristTF.setIdentity();

	std::vector<Eigen::Vector6d> angles;

	Eigen::Isometry3d goal = Eigen::Isometry3d::Identity();
	goal.translate(Eigen::Vector3d(transform.getOrigin().x(),
								   transform.getOrigin().y(),
								   transform.getOrigin().z()));
	goal.rotate(Eigen::Quaterniond(transform.getRotation().getW(),
								   transform.getRotation().getX(),
								   transform.getRotation().getY(),
								   transform.getRotation().getZ()));

	kr5::IK(goal, angles);
	int numSolutions = angles.size();
	kr5::filterByLimits(angles);
	int numValidSolutions = angles.size();

	if (numValidSolutions != numSolutions)
	{
		std::cout << "Rejected " << numSolutions-numValidSolutions << " solutions due to joint limit violations." << std::endl;
	}

	if (angles.size() < 1)
	{
		std::cout << "Unable to find a solution." << std::endl;
	}
	else
	{
		for (int i = 0; i < angles.size(); ++i)
		{
			for (int j = 0; j < 6; ++j)
			{
				std::cout << "\t" << angles[i][j];
			}
			std::cout << std::endl;
		}
		ros::Rate r(0.25);
		r.sleep();
		while(ros::ok())
		{
//			std::vector<std::array<double,3> > wristAngles;
//			wristRotation(q.matrix(), wristAngles);
//			for (int i = 0; i < wristAngles.size(); ++i)
			for (int i = 0; i < angles.size(); ++i)
			{
				for (int j = 0; j < 6; ++j)
				{
					js.position[j] = angles[i][j];
				}


//				js.position[0] = 0.0;
//				js.position[1] = 0.0;
//				js.position[2] = 0.5;
//				for (int j = 3; j < 6; ++j)
//				{
//					js.position[j] = wristAngles[i][j-3];
//				}

				Eigen::Matrix3d armRot = kr5::dhRotation(kr5::alpha1,kr5::theta1,angles[i][0]) *
										 kr5::dhRotation(kr5::alpha2,kr5::theta2,angles[i][1]) *
										 kr5::dhRotation(kr5::alpha3,kr5::theta3,angles[i][2]) *
										 Eigen::AngleAxisd(-M_PI/2.0, Eigen::Vector3d::UnitY());
//				Eigen::Matrix3d armRot = kr5::dhRotation(kr5::alpha1,kr5::theta1,0) *
//										 kr5::dhRotation(kr5::alpha2,kr5::theta2,0) *
//										 kr5::dhRotation(kr5::alpha3,kr5::theta3,0.5) *
//										 Eigen::AngleAxisd(-M_PI/2.0, Eigen::Vector3d::UnitY());

				Eigen::Quaterniond q2 = Eigen::Quaterniond(armRot.transpose() * q);
				wristTF.setRotation(tf::Quaternion(q2.x(),q2.y(),q2.z(),q2.w()));

				js.header.stamp = ros::Time::now();
				transform.stamp_ = ros::Time::now();
				wristTF.stamp_ = ros::Time::now();
				jsPub.publish(js);
				tb.sendTransform(transform);
				tb.sendTransform(wristTF);
				ros::spinOnce();
				r.sleep();
			}
			ros::spinOnce();
		}
	}
	return 0;
}
