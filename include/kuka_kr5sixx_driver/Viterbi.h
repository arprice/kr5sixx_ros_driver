/**
 * \file Viterbi.h
 * \brief
 *
 * \author Andrew Price
 * \date 2014-12-16
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VITERBI_H
#define VITERBI_H


#include <vector>
#include <iostream>
#include <limits>
#include <assert.h>

struct CostAccumulator
{
	double cost;
	int predecessor;

	CostAccumulator()
	{
		cost = std::numeric_limits<double>::max();
		predecessor = -1;
	}
};


/**
 * @brief viterbi Computes the indices for the minimum cost path through a trellis structure
 * @tparam T Elemental type for states within the steps of the trellis
 * @param trellis
 * @param stateCostFn Function pointer or functor for evaluating the cost of a single state in the trellis.
 *  Of the form \code double StateCostFn(const T&) \endcode.
 * @param transitionCostFn Function pointer or functor for evaluating the cost of a state-action-state transition in the trellis.
 *  Of the form \code double TransitionCostFn(const T&, const T&) \endcode.
 * @return Vector of indices for the minimum cost path through a trellis structure
 */
template <class T, typename StateCostFn, typename TransitionCostFn>
std::vector<int> viterbi(const std::vector<std::vector<T> >& trellis, const StateCostFn stateCostFn, const TransitionCostFn transitionCostFn, const double maxAllowedTransitionCost = std::numeric_limits<double>::max())
{
	const int N = trellis.size();

	std::vector<std::vector<CostAccumulator> > costGraph;
	costGraph.reserve(N);

#ifdef GENERATE_TEXT_GRAPH
	std::cerr << "digraph g \n{\n\trankdir=\"LR\";\n\tsplines=\"line\";" << std::endl;
#endif

	for (int n = 0; n < N; ++n)
	{
		const int I = trellis[n].size();
		assert(I > 0);

		std::vector<CostAccumulator> costGraphSlice;
		costGraphSlice.reserve(I);

		for (int i = 0; i < I; ++i)
		{
			CostAccumulator acc; // Accumulated cost

			double stateCost = stateCostFn(trellis[n][i]);

#ifdef GENERATE_TEXT_GRAPH
			std::cerr << "\tn" << n << "_" << i << "[label=\"" << stateCost << "\"];" << std::endl;
#endif

			if (n > 0)
			{
				const int J = trellis[n-1].size();
				assert(J > 0);
				for (int j = 0; j < J; ++j)
				{
					double transCost = transitionCostFn(trellis[n-1][j], trellis[n][i]);
					if (transCost > maxAllowedTransitionCost) { continue; }
					double netCost = costGraph[n-1][j].cost + transCost + stateCost;

#ifdef GENERATE_TEXT_GRAPH
					std::cerr << "\tn" << n-1 << "_" << j << "->" << "n" << n << "_" << i << "[label=\"" << transitionCostFn(trellis[n-1][j], trellis[n][i]) << "\"];" << std::endl;
#endif

					if (netCost < acc.cost)
					{
						acc.cost = netCost;
						acc.predecessor = j;
					}
				}

				if (-1 == acc.predecessor)
				{
					// No solution was found that satisfied the cost.
					return std::vector<int>();
				}

			}
			else
			{
				// For first state, no prior transition cost
				acc.cost = stateCost;
			}
			costGraphSlice.push_back(acc);

		}
		costGraph.push_back(costGraphSlice);
	}

	// Get the minimum cost path
	CostAccumulator minFinalCost;
	for (int i = 0; i < costGraph[N-1].size(); ++i)
	{
		if (costGraph[N-1][i].cost < minFinalCost.cost)
		{
			minFinalCost.cost = costGraph[N-1][i].cost;
			minFinalCost.predecessor = i;
		}
	}
	assert(-1 != minFinalCost.predecessor);

	// Backtrack the solution
	std::vector<int> minCostPath(N);
	for (int i = N-1; i >= 0; --i)
	{
		minCostPath[i] = minFinalCost.predecessor;
		minFinalCost = costGraph[i][minFinalCost.predecessor];
	}

#ifdef GENERATE_TEXT_GRAPH
	// Bold edges of selected path
	std::cerr << std::endl;
	for (int n = 1; n < N; ++n)
	{
		std::cerr << "\tn" << n-1 << "_" << minCostPath[n-1] << "->" << "n" << n << "_" << minCostPath[n] << "[penwidth=\"3\"];" << std::endl;
	}
	std::cerr << "}" << std::endl;
#endif

	return minCostPath;
}

#endif // VITERBI_H
