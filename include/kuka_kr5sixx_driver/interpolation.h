/**
 * \file interpolation.h
 * \brief
 *
 * \author Andrew Price
 * \date 2014-12-20
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#include <Eigen/Geometry>

/**
 * @brief Performs cubic spline interpolation given boundary positions and velocities
 * @param x0 Inital conditions of state
 * @param xT Final conditions of state
 * @param T Time in which to reach final state
 * @param deltaT Interpolation stepsize
 * @return A trajectory with each element containing position, velocity, and acceleration at that time step
 */
std::vector<Eigen::Vector3d> sisoCubicSpline(const Eigen::Vector2d& x0, const Eigen::Vector2d& xT, const double T, const double deltaT)
{
	const Eigen::Vector2d deltaX = xT-x0;
	double c1, c2, c3, c4;

	c1 = -12.0/(T*T*T)*(-0.5*xT[1]*T + deltaX[0]);
	c2 = 1.0/T * (0.5*c1*(T*T)-deltaX[1]);
	c3 = x0[1];
	c4 = x0[0];

	const int numSteps = static_cast<int>((T/deltaT)) + 1;

	std::vector<Eigen::Vector3d> traj;
	traj.resize(numSteps);
	for (int i = 0; i < numSteps; ++i)
	{
		double t = deltaT * static_cast<double>(i);
		traj[i][0] = 1.0/6.0*c1*(t*t*t) - 1.0/2.0*c2*(t*t) + c3*t + c4;
		traj[i][1] = 1.0/2.0*c1*(t*t) - c2*t + c3;
		traj[i][2] = c1*t - c2;
	}
	//assert(x0.isApprox(traj[0], 1e-9));
	//assert(xT.isApprox(traj[numSteps-1], 1e-3));

	return traj;
}

/**
 * @brief Performs minimum jerk spline interpolation given boundary positions and velocities
 * @param x0 Inital conditions of state
 * @param xT Final conditions of state
 * @param T Time in which to reach final state
 * @param deltaT Interpolation stepsize
 * @return A trajectory with each element containing position, velocity, and acceleration at that time step
 */
std::vector<Eigen::Vector3d> sisoQuinticSpline(const Eigen::Vector3d& x0, const Eigen::Vector3d& xT, const double T, const double deltaT)
{
	Eigen::Matrix<double, 6, 1> C;

	double T2 = pow(T, 2);
	double T3 = pow(T, 3);
	double T4 = pow(T, 4);
	double T5 = pow(T, 5);

	Eigen::Matrix<double, 6, 6> A;
	A << 1, 0, 0, 0, 0, 0,
		 0, 1, 0, 0, 0, 0,
		 0, 0, 2, 0, 0, 0,
		 1, T, T2, T3, T4, T5,
		 0, 1, 2.0*T, 3.0*T2, 4.0*T3, 5.0*T4,
	     0, 0, 2, 6.0*T, 12.0*T2, 20.0*T3;

	Eigen::Matrix<double, 6, 1> X;
	X << x0, xT;

	C = A.inverse() * X;

	const int numSteps = static_cast<int>((T/deltaT)) + 1;

	std::vector<Eigen::Vector3d> traj;
	traj.resize(numSteps);
	for (int i = 0; i < numSteps; ++i)
	{
		double t = deltaT * static_cast<double>(i);
		double t2 = pow(t, 2);
		double t3 = pow(t, 3);
		double t4 = pow(t, 4);
		double t5 = pow(t, 5);

		traj[i][0] = C[5]*t5 + C[4]*t4 + C[3]*t3 + C[2]*t2 + C[1]*t + C[0];
		traj[i][1] = 5.0*C[5]*t4 + 4.0*C[4]*t3 + 3.0*C[3]*t2 + 2.0*C[2]*t + C[1];
		traj[i][2] = 20.0*C[5]*t3 + 12.0*C[4]*t2 + 6.0*C[3]*t + 2.0*C[2];
	}

	return traj;
}

std::vector<Eigen::Vector3d> sisoQuinticSpline(const Eigen::Vector2d& x0, const Eigen::Vector2d& xT, const double T, const double deltaT)
{
	return sisoQuinticSpline(Eigen::Vector3d(x0[0], x0[1], 0), Eigen::Vector3d(xT[0], xT[1], 0), T, deltaT);
}


#endif // INTERPOLATION_H
