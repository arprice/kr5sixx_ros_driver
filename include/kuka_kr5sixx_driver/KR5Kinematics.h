/**
 * \file KR5Kinematics.h
 * \brief
 *
 * \author Andrew Price
 * \date 8 20, 2014
 *
 * \copyright
 *
 * Copyright (c) 2014, Georgia Tech Research Corporation
 * All rights reserved.
 *
 * This file is provided under the following "BSD-style" License:
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 *   copyright notice, this list of conditions and the following
 *   disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef KR5KINEMATICS_H
#define KR5KINEMATICS_H

#include <vector>
#include <array>
#include <math.h>
#include <cmath>

#include <Eigen/Core>
#include <Eigen/Geometry>

//#define ENFORCE_JOINT_LIMITS

namespace Eigen
{
typedef Matrix<double, 6, 1> Vector6d;
}

namespace kr5 {

// D-H Parameters

// Interesting Distances
const double d1 = 0.333;//0.335;
const double r1 = 0.075;
const double r2 = 0.270;
const double r3 = 0.090;
const double d4 = 0.293;//0.295;
const double t6 = 0.080;

// Uninteresting Distances
const double d2 = 0.0;
const double d3 = 0.0;
const double r4 = 0.0;
const double d5 = 0.0;
const double r5 = 0.0;
const double d6 = 0.0;
const double r6 = 0.0;

const double alpha1 = -M_PI/2.0;
const double alpha2 = 0.0;
const double alpha3 =  M_PI/2.0;
const double alpha4 = -M_PI/2.0;
const double alpha5 =  M_PI/2.0;
const double alpha6 =  M_PI/2.0;

const double theta1 = 0.0;
const double theta2 = -M_PI/2.0;
const double theta3 = M_PI;
const double theta4 = 0.0;
const double theta5 = 0.0;
const double theta6 = 0.0;


const double rho = sqrt(r3*r3+d4*d4);
const double phi = atan2(d4,r3);

static const double minAngle[6] = {-2.967059728, -1.745329252, -3.647738137, -3.316125579, -2.094395102, -6.24827872};
static const double maxAngle[6] = {2.967059728, 2.35619449, 0.959931089, 3.316125579, 2.094395102, 6.24827872};

typedef Eigen::Vector6d Vector6d;

class SolutionSort
{
public:
	const Eigen::Vector6d qTarget;
	const Eigen::Vector6d mWeights;

	SolutionSort(const Eigen::Vector6d& qPrev, const Eigen::Vector6d& weights = Eigen::Vector6d((Eigen::Vector6d() << 1,1,1,1,1,1).finished()))
		: qTarget(qPrev), mWeights(weights) {}

	inline double weightedDistance(const Eigen::Vector6d& a, const Eigen::Vector6d& b)
	{
		return ((a-b).array() * mWeights.array()).matrix().norm();
	}

	bool operator() (const Eigen::Vector6d& q1, const Eigen::Vector6d& q2)
	{
		return weightedDistance(q1,qTarget) < weightedDistance(q2,qTarget);
	}
};

Eigen::Matrix3d dhRotation(const double alpha, const double thetaOffset, const double theta);

void wristIK(const Eigen::Vector3d& goal, std::vector<std::array<double,3> >& jointAngles);

void IK(const Eigen::Isometry3d& goal, std::vector<Vector6d>& jointAngles);

void filterByLimits(std::vector<Vector6d>& jointAngles);

} // namespace kr5

#endif // KR5KINEMATICS_H
